<?php
/*
  *  EMMANUEL TORRES SERVÍN
  *  emmanueltorres_servin@hotmail.com
  */
namespace Model;
// clases externas de composer
require_once 'vendor/autoload.php'; // para ocupar lo necesario de composer
use Verot\Upload\Upload; // es obligatorio usar el namespace para cargar la clase

class Redimensionar
{
    // -- Redimensionar imagen
    function redimensionarNuevo($imagenO, $x, $y, $opcion)
    {
        //CREANDO Objeto
        $imagenNueva  = new Upload($imagenO);
        // -- Ubicación donde se guarda la imagen nueva
        $path = 'uploads/';

        if (!$imagenNueva) {
            // La imagen no se ha subido correctamente, vuelve a intentarlo
            redirect('Vista.php?error=no-uploaded');
        }

        //NUEVA VERSIÓN DE LA IMAGEN

        //permitiendo cambiar el tamaño de la nueva imagen
        $imagenNueva->image_resize= true;

        switch ($opcion){
            case 1:
                echo "<HR>ENTRO EN 1<HR>";
                $size_x = $x;//para cambiar el ancho
                $imagenNueva->image_x= $size_x;
                $imagenNueva->image_y= $y;
                break;
            case 2:
                echo "<HR>ENTRO EN 2<HR>";
                $size_x = $x;//para cambiar el ancho
                $imagenNueva->image_x= $size_x;
                $imagenNueva->image_y= round($size_x * $y / $x);//regla de tres
                break;
            case 3:
                echo "<HR>ENTRO EN 3<HR>";
                $size_y = $y;//para cambiar el ancho
                $imagenNueva->image_y= $size_y;
                $imagenNueva->image_x= round($size_y * $x / $y);//regla de tres
                break;
        }

        //REDIMENSIONANDO A PARTIR DE X
        $imagenNueva->file_new_name_body = sprintf('%spx_%s', 'EJEX:'.$imagenNueva->image_x." X ".$imagenNueva->image_y, time());//cambiar nombre de la imagen

        //para obtener un alto proporcional al nuevo ancho sin perder la dimensión de la imagen
        $imagenNueva->process($path);
        //$imagenNueva es el produto final
        if ($imagenNueva->processed) {
            echo "IMEGEN REDIMENSIONADA<br>";
            $imagenNueva->clean();//para cerrar el proceso
        } else {
            echo sprintf('Error: %s<br>', $imagenNueva->error);
        }
        echo "OPCIÓN: ". $opcion;
        echo "<br> Eje X:".$x;
        echo "<br> Eje Y:".$y;
        echo '<br>Proceso finalizado';
    }

    // -- Generar código aleatorio
    static function crearCodigo ($n){
        // -- Cadena que contiene los caracteres para crear el código
        $str = "ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz123456789";
        $codigo = "";
        // -- Recorrer hasta "n", generar una posición aleatoria y concatenarla a la variable $codigo
        for ($i = 0; $i < $n; $i++) {
            $codigo .= substr($str, rand(0, 62), 1);
        }

        echo "El código generado es: ".$codigo;
        //return $codigo;
    }

    // -- Quitar los acentos de una cadena de texto
    static public function crearSlug ($cadena){
        // -- Cadena para comparar valores
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        // -- Cadena para reemplazar valores originales
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        // -- Reemplzar valores entre cadenas
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        // -- hacer minusculas
        $cadena = strtolower($cadena);
        //return utf8_encode($cadena);
        echo utf8_encode($cadena);
    }
}
