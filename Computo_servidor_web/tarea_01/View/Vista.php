<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Redimensionar</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body class="bg-light">

<!-- Navbar -->
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href=""></a>
        <a class="p-2 text-dark" href=""></a>
    </nav>
</div>
<!-- ends navbar -->

<!-- Formulario y contenido -->
<div class="container">
    <div class="row">
        <div class="offset-xl-3 col-xl-6 py-5">
            <h2 class="mb-4">Redimensionar imágenes</h2>
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <form method="POST" action="/redimensionar/?controller=Redimensionar&action=create"
                          enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="file">Selecciona una opción</label>
                            <select class="form-control" name="opcion">
                                <option value="1">Mismo Tamaño en X y Y</option>
                                <option value="2">En base a X</option>
                                <option value="3">En base a Y</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="file">Valor de X</label>
                            <input type="text" class="form-control" name="ejeX">
                        </div>
                        <div class="form-group">
                            <label for="file">Valor de Y</label>
                            <input type="text" class="form-control" name="ejeY">
                        </div>
                        <div class="form-group">
                            <label for="file">Selecciona una imagen</label>
                            <input type="file" class="form-control form-control-file" name="imagen" id="imagen"
                                   required>
                        </div>
                        <button class="btn btn-success" type="submit">Redimensionar</button>
                    </form>
                </div>
                <div class="card-footer text-center">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="offset-xl-3 col-xl-6 py-5">
            <h2 class="mb-4">Quitar acentos</h2>
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <form method="POST" action="/redimensionar/?controller=Redimensionar&action=crearSlug">
                        <div class="form-group">
                            <input type="text" name="texto" class="form-control" placeholder="Texto para transformar" required>
                        </div>
                        <button class="btn btn-success" type="submit">Aceptar</button>
                    </form>
                </div>
                <div class="card-footer text-center">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="offset-xl-3 col-xl-6 py-5">
            <h2 class="mb-4">Generar Código aleatorio</h2>
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <form method="POST" action="/redimensionar/?controller=Redimensionar&action=crearCodigo"
                          enctype="multipart/form-data">
                        <button class="btn btn-success" type="submit">Aceptar</button>
                    </form>
                </div>
                <div class="card-footer text-center">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ends contenido -->

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

</body>
</html>