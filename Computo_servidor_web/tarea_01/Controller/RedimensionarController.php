<?php
/*
  *  EMMANUEL TORRES SERVÍN
  *  emmanueltorres_servin@hotmail.com
  */
require_once "Model/Redimensionar.php";
use Model\Redimensionar;

class RedimensionarController
{
    public function index(){
        require_once "View/Vista.php";
    }
    public function create()
    {
        if (isset($_FILES['imagen'])) // Validar la existencia del parametro con el archivo
        {
            // -- Variables opcionales para usuario (Por si quiere redimensionar de forma personalizada)
            $opcion = $_POST["opcion"];

            if(isset($_POST["ejeX"]))
                $ejeX = $_POST["ejeX"];

            if(isset($_POST["ejeY"]))
                $ejeY = $_POST["ejeY"];

            $imagenOrignal = $_FILES['imagen']; // archivo temporal en espera de ser procesado
            //OBTENIENDO MEDIDAS ORIGINALES, SÓLO SE ADMITEN FORMATOS PNG O JPEG
            if($_FILES['imagen']['type']=='image/jpeg')
                $original=imagecreatefromjpeg($_FILES['imagen']['tmp_name']);
            elseif ($_FILES['imagen']['type']=='image/png')
                $original=imagecreatefrompng($_FILES['imagen']['tmp_name']);
            else
                redirect('Vista.php?error=no-image');

            /*
             *  SWITCH DE OPCIONES
             *  1   TAMAÑO POR DEFAULT ( X Y Y DEL MISMO TAMAÑO)
             *  2   TAMAÑO EN BASE A X
             *  3   TAMAÑO EN BASE A Y
             */
            switch ($opcion){
                case 1:
                    //OBTENIENDO ANCHO Y ALTO DE LA IMAGEN ORIGINAL
                    $original_x=$ejeX;
                    $original_y=$ejeX;
                    break;

                case 2:
                    $original_x=imagesx($original);
                    $original_y=$original_x;
                    break;

                case 3:
                    $original_y=imagesy($original);
                    $original_x=$original_y;
                    break;
            }
            $r=new Redimensionar();
            $r->redimensionarNuevo($imagenOrignal,$original_x,$original_y,$opcion);
        }
        else
        {
            redirect('Vista.php?error=no-image');
        }
    }

    public function crearCodigo(){
        $tamanioCodigo = 10;
        Redimensionar::crearCodigo($tamanioCodigo);
    }

    public function crearSlug(){
        $cadena = $_POST["texto"];
        Redimensionar::crearSlug($cadena);
    }

}

