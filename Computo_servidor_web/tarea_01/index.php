
<?php
/*
  *  EMMANUEL TORRES SERVÍN
  *  emmanueltorres_servin@hotmail.com
  */
if(isset($_GET["controller"])&& isset($_GET["action"])){

    $controller = $_GET["controller"];
    $action = $_GET["action"];

    $clase = $controller . "Controller";
    require_once("Controller/".$clase.".php");
    session_start();
    $instancia = new $clase(); //
    $instancia->{$action}();
}else{
    echo "Error en la peticion";
}